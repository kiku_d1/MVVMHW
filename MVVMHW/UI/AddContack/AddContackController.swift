//
//  AddContackController.swift
//  MVVMHW
//
//  Created by diwka on 22/6/22.
//

import Foundation
import UIKit
class AddContackController: UIViewController {
    
    private lazy var nameTextField: UITextField = {
        let view = UITextField()
        view.placeholder = "Введите имя"
        view.font = .systemFont(ofSize: 24, weight: .regular)
        view.borderStyle = .bezel
        return view
    }()
    
    private lazy var LastNameTextField: UITextField = {
        let view = UITextField()
        view.placeholder = "Введите фамилию"
        view.font = .systemFont(ofSize: 24, weight: .regular)
        view.borderStyle = .bezel
        return view
    }()
    
    private lazy var numberTextField: UITextField = {
        let view = UITextField()
        view.placeholder = "Введите номер"
        view.font = .systemFont(ofSize: 24, weight: .regular)
        view.borderStyle = .bezel
        return view
    }()
    
    private lazy var saveButton: UIButton = {
        let view = UIButton()
        view.setTitle("готово", for: .normal)
        view.titleLabel?.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        view.addTarget(self, action: #selector(sendContack(view:)), for: .touchUpInside)
        view.setTitleColor(UIColor.black, for: .normal)
        return view
    }()
    
    @objc func sendContack(view: UIButton){
        MainModel.shared.getData(model: Item(name: nameTextField.text ?? "" , lastName: LastNameTextField.text ?? "" , number: numberTextField.text ?? "" ))
        navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        setupSubviews()
        view.backgroundColor = .white
    }
    
    private func setupSubviews(){
        view.addSubview(saveButton)
        saveButton.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top)
            make.right.equalTo(view.safeArea.right).offset(-5)
            make.height.equalTo(45)
            make.width.equalTo(100)
        }
        
        view.addSubview(nameTextField)
        nameTextField.snp.makeConstraints { make in
            make.top.equalTo(saveButton.snp.bottom).offset(10)
            make.left.equalTo(view.safeArea.left).offset(20)
            make.height.equalTo(60)
            make.width.equalTo(250)
        }
        
        view.addSubview(LastNameTextField)
        LastNameTextField.snp.makeConstraints { make in
            make.top.equalTo(nameTextField.snp.bottom).offset(20)
            make.left.equalTo(view.safeArea.left).offset(20)
            make.height.equalTo(60)
            make.width.equalTo(250)
        }
        
        view.addSubview(numberTextField)
        numberTextField.snp.makeConstraints { make in
            make.top.equalTo(LastNameTextField.snp.bottom).offset(20)
            make.left.equalTo(view.safeArea.left).offset(20)
            make.height.equalTo(60)
            make.width.equalTo(250)
        }
    }
}
