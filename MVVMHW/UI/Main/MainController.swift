//
//  ViewController.swift
//  MVVMHW
//
//  Created by diwka on 22/6/22.
//

import UIKit
import SnapKit
class MainController: UIViewController {

    private lazy var addButton: UIButton = {
        let view = UIButton()
        view.setTitle("+", for: .normal)
        view.titleLabel?.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        view.setTitleColor(UIColor.black, for: .normal)
        view.addTarget(self, action: #selector(clickButton(view:)), for: .touchUpInside)
        return view
    }()
    
    private lazy var contckTable: UITableView = {
        let view = UITableView()
        view.dataSource = self
        view.delegate = self
        view.register(MainCell.self, forCellReuseIdentifier: "MainCell")
        return view
    }()
    
    @objc func clickButton(view: UIButton){
        navigationController?.pushViewController(AddContackController(), animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        contckTable.reloadData()
    }
   private func setupSubviews(){
       view.addSubview(addButton)
       addButton.snp.makeConstraints { make in
           make.top.equalTo(view.safeArea.top)
           make.right.equalTo(view.safeArea.right).offset(-10)
           make.height.width.equalTo(45)
       }
       view.addSubview(contckTable)
       contckTable.snp.makeConstraints { make in
           make.top.equalTo(addButton.snp.bottom)
           make.right.equalTo(view.safeArea.right)
           make.left.equalTo(view.safeArea.left)
           make.bottom.equalTo(view.safeArea.bottom)
       }
    }
    private func bindViewModel(){
        
    }
}
extension MainController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MainModel.shared.saveContack.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainCell", for: indexPath) as! MainCell
        let model = MainModel.shared.saveContack[indexPath.row]
        cell.fill(model: model)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    
}

