//
//  Item.swift
//  MVVMHW
//
//  Created by diwka on 22/6/22.
//

import Foundation
struct Item {
    let name: String
    let lastName: String
    let number: String
}
