//
//  Dynamic.swift
//  MVVMHW
//
//  Created by diwka on 22/6/22.
//

import Foundation
class Dynamic<T> {
    typealias Listner = (T) -> Void
    private var listner: Listner?
    
    func bind(_ listner: Listner?){
        self.listner = listner
    }
    
    var value: T {
        didSet{
            listner?(value)
        }
    }
    init (_ v: T){
        value = v
    }
}
