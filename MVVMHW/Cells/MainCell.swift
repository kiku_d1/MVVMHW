//
//  MainCell.swift
//  MVVMHW
//
//  Created by diwka on 22/6/22.
//

import Foundation
import UIKit
class MainCell: UITableViewCell {
    
    private lazy var nameLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 24, weight: .regular)
        return view
    }()
    
    private lazy var lastNameLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 24, weight: .regular)
        return view
    }()
    
    private lazy var numberLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 24, weight: .regular)
        return view
    }()
    
    override func layoutSubviews() {
        
        setupSubviews()
    }
    func fill(model: Item){
        self.nameLabel.text = model.name
        self.lastNameLabel.text = model.lastName
        self.numberLabel.text = model.number
    }
    private func setupSubviews(){
        addSubview(nameLabel)
        nameLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(5)
            make.left.equalToSuperview().offset(10)
            make.height.equalTo(30)
        }
        addSubview(lastNameLabel)
        lastNameLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(5)
            make.left.equalTo(nameLabel.snp.right).offset(10)
            make.height.equalTo(30)
        }
        addSubview(numberLabel)
        numberLabel.snp.makeConstraints { make in
            make.top.equalTo(lastNameLabel.snp.bottom).offset(5)
            make.left.equalToSuperview().offset(10)
            make.height.equalTo(30)
        }
    }
}
